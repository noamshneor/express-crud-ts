import log from "@ajar/marker";
import { Request, Response, NextFunction } from "express";
import { addToErrorLOG } from "../db/logFiles.js";
import { HttpError } from "../errors/http.error.js";
import { NotfoundError } from "../errors/notfound.error.js";
import { pathErrorLOG } from "../index.js";
const { White, Reset, Red } = log.constants;
const { NODE_ENV } = process.env;

export const error_handler_log = (
    err: HttpError,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    addToErrorLOG(err, pathErrorLOG);
    next(err);
};

export const error_handler_status = (
    err: HttpError,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    err.status = err.status || 500;
    if (NODE_ENV !== "production")
        res.status(err.status).json({
            status: err.status,
            message: err.message,
            stack: err.stack,
        });
    else
        res.status(err.status).json({
            status: err.status,
            message: "internal server error...",
        });
    next(err);
};

export const not_found = (req: Request, res: Response, next: NextFunction) => {
    log.info(`url: ${White}${req.url}${Reset}${Red} not found...`);
    next(new NotfoundError(req.url));
    //res.status(404).json({ status: `url: ${req.url} not found...` });
};
