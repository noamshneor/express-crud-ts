import { Request } from "express";
import user_model from "./user.model.js";

export async function newUser(req: Request) {
    return await user_model.create(req.body);
}

export async function getAllUsers() {
    return await user_model.find().select(`_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
}

export async function getUser(req: Request) {
    return await user_model.findById(req.params.id);
}

export async function updateUser(req: Request) {
    return await user_model.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        upsert: false,
    });
}

export async function deleteUser(req: Request) {
    return await user_model.findByIdAndRemove(req.params.id);
}

export async function getPaginate(req: Request) {
    const pageNum = parseInt(req.params.pageNum);
    const nPerPage = parseInt(req.params.nPerPage);
    return await user_model
        .find()
        .skip(pageNum > 0 ? (pageNum - 1) * nPerPage : 0)
        .limit(nPerPage).select(`-_id 
        first_name 
        last_name 
        email
        phone`);
}

export function generateID() {
    return Math.random().toString(36).substring(2);
}
