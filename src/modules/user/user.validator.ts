import Joi from "joi";
const schema = Joi.object({
    first_name: Joi.string().alphanum().min(2).max(30).required(),
    last_name: Joi.string().alphanum().min(2).max(30).required(),
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),
    phone: Joi.string()
        .pattern(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)
        .required(),
});
// ^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$
export default schema;
