import { NextFunction, Request, Response } from "express";
import { pathHttpLOG, pathErrorLOG } from "../index.js";
import { addToHttpLOG, checkLOG } from "../db/logFiles.js";
import { generateID } from "../modules/user/user.service.js";

export async function setID(req: Request, res: Response, next: NextFunction) {
    req.id = generateID();
    next();
}

export async function getLogs(req: Request, res: Response, next: NextFunction) {
    await checkLOG(pathHttpLOG);
    await checkLOG(pathErrorLOG);
    next();
}

export function httpLogger(req: Request, res: Response, next: NextFunction) {
    addToHttpLOG(req, pathHttpLOG);
    next();
}
