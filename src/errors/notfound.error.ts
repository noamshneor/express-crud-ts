import { HttpError } from "./http.error.js";

export class NotfoundError extends HttpError {
    constructor(url: string) {
        super(404, `url: ${url} not found...`);
    }
}
