import fs from "fs/promises";
import { Request } from "express";
import { HttpError } from "../errors/http.error.js";

export async function checkLOG(pathLOG: string) {
    try {
        const file = await fs.readFile(pathLOG, "utf-8");
        return file;
    } catch (error) {
        fs.writeFile(pathLOG, "LOG FILE", "utf-8"); // if file not exists
        return "LOG FILE";
    }
}

export async function addToHttpLOG(req: Request, pathHttpLOG: string) {
    const newStr = `${await fs.readFile(pathHttpLOG, "utf-8")}
  ${req.id} --> ${req.method} :: ${req.originalUrl} >> ${Date.now()}`;
    fs.writeFile(pathHttpLOG, newStr, "utf-8");
}

export async function addToErrorLOG(err: HttpError, pathErrorLOG: string) {
    const newStr = `${await fs.readFile(pathErrorLOG, "utf-8")}
  ${err.status} --> ${err.message}
  ${err.stack ? err.stack : ""} 
  >> ${Date.now()}`;
    fs.writeFile(pathErrorLOG, newStr, "utf-8");
}
